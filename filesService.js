// requires...
const fs = require('fs');
const path = require('path');
// constants...
const allowedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];


function createFile (req, res, next) {
  // Your code to create the file.
const {filename, content} = req.body;

  if(!filename){
    next({message: 'Please specify "filename" parameter', status :"400"});
  } else if (!content) {
    next({message: 'Please specify "content" parameter', status :"400"});
  }else if(!allowedExtensions.includes(path.extname(req.body.filename).split('.').pop())){ 
    next({message: 'Please specify "extension" parameter', status :"400"});
  } else {
    fs.writeFile(`files/${filename}`, `${content}`,  (err) => {
      if(err) {
        next(err)
      }
      next({message: 'File created successfully', status: '200'})
    })
  }
}

function getFiles (req, res, next) {
  // Your code to get all files.
  const filesArray = [];
  fs.readdir('files/', (err, files) => {
    files.forEach(file => {
      filesArray.push(file);
    });

    if (err) {
      next({message: "Server error", status: 500})
      return
    } else {
      res.status(200).json({
        "message": "Success",
        "files": filesArray}
      );
    }

  });

}

const getFile = (req, res, next) => {
  // Your code to get all files.
  
  if (allowedExtensions.includes(path.extname(req.params.filename).split('.').pop())){
    fs.readFile(`files/${req.params.filename}`, (err,data) => {
      let uploadDate = '';

     if (!fs.existsSync(`files/${req.params.filename}`)){
      //  res.status(400).json({"message": `No file with '${req.body.filename}' filename found`});
       next({message: `No file with '${req.params.filename}' filename found`, status: 400})

     } else {
      
      fs.stat('./', (err, stats) => {
        if (err) {throw err;}
        else {
          uploadDate = stats.birthtime
        }
      });
      res.status(200).json({
        "message": "Success",
        "filename": `${req.params.filename}`,
        "content": `${data}`,
        "extension": `${path.extname(req.params.filename).split('.').pop()}`,
        "uploadedDate": `${uploadDate}`
      })   
     }
 }) 
}else {
    res.status(400).json({"message": "Server error"});
  }
  

}

// Other functions - editFile, deleteFile

// const deleteFile = (req, res, next) => {
//   if (allowedExtensions.includes(path.extname(req.body.filename))){
//     fs.unlink(`files/${req.body.filename}`, (err) => {
//       if (err) {
//         res.status(400).json({"message": `No file with '${req.body.filename}' filename found`});
//         throw err
//       } else {
//         res.status(200).json({ "message": "File deleted successfully" });
//       } 
//     })
//   } else {
//     res.status(500).json({"message": "Server error"});
//   }
// }

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  // deleteFile
};
